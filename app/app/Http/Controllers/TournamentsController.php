<?php

namespace App\Http\Controllers;

use App\Tournament;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Log;


class TournamentsController extends Controller
{

    /*
      Estadisticas de los equipos en un torneo
      las cuales se obtienen solo de las jornadas
      regulares (omiten las jornadas extras), el
      arreglo tiene que estár ordenado del equipo
      que tiene mas puntos(points) al que menos
      [
        {
        {
          "id": 1,                     // id de equipo
          "name": "Leones",            // Nombre de equipo
          "goals": 4,                  // Goles totales del equipo en el torneo
          "received_goals": 5,         // Goles recibidos del equipo en el torneo
          "difference_of_goals": -1,   // Diferencia de goles (goals-received_goals)
          "matches_played": 6,         // El numero total de "matches" con resultado donde participa el equipo en el torneo
          "matches_won": 1,            // El numero total de "matches" con resultado donde participa el equipo en el torneo donde el equipo sea ganador
          "draw_matches": 2,           // El numero total de "matches" con resultado donde participa el equipo en el torneo donde el resultado sea empate
          "matches_lost": 3,           // El numero total de "matches" con resultado donde participa el equipo en el torneo donde el equipo sea el perdedor
          "points": 5,                 // Puntos obtenidos por el equipo en el torneo (3 puntos por partid ganado 1 punto por partido empatado y 0 puntos por partido perdido)
        }
        }
      ]
    */

    public function generalTable(Tournament $tournament)
    {
      return [];
    }


}
